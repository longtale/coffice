FROM centos:7
#维护者信息
MAINTAINER sun.chen@aliyun.com
#安装wget
RUN yum -y install wget
#下载安装包
#RUN wget https://sourceforge.net/projects/openofficeorg.mirror/files/4.1.8/binaries/zh-CN/Apache_OpenOffice_4.1.8_Linux_x86-64_install-rpm_zh-CN.tar.gz
COPY Apache_OpenOffice_4.1.8_Linux_x86-64_install-rpm_zh-CN.tar.gz   Apache_OpenOffice_4.1.8_Linux_x86-64_install-rpm_zh-CN.tar.gz
RUN chmod 755 Apache_OpenOffice_4.1.8_Linux_x86-64_install-rpm_zh-CN.tar.gz
#解压安装包
RUN tar -xvf Apache_OpenOffice_4.1.8_Linux_x86-64_install-rpm_zh-CN.tar.gz
#安装openOffice
RUN yum install -y zh-CN/RPMS/*.rpm
#安装JDK
RUN yum install -y java-1.8.0-openjdk.x86_64
#清除yum缓存
#RUN yum clean all
#删除压缩包
RUN rm -f Apache_OpenOffice_4.1.6_Linux_x86-64_install-rpm_zh-CN.tar.gz
#删除解压缩的文件
RUN rm -Rf zh-CN
#启动服务
CMD soffice -headless -nofirststartwizard  -accept="socket,host=0.0.0.0,port=8100;urp;"
#安装应用
COPY target/conversion-1.0-SNAPSHOT.jar /app/app.jar
#创建文件上传和转换目录
RUN mkdir -p /app/file/uploadPath/
RUN mkdir -p /app/file/pdfPath/
RUN mkdir -p /app/file/pngPath/
RUN chmod -R 755 /app/file/
ENTRYPOINT ["java","-Xmx256m","-jar", "/app/app.jar"]

