package com.sunchen.coffice.service;

import org.jodconverter.core.DocumentConverter;
import org.jodconverter.core.document.DefaultDocumentFormatRegistry;
import org.jodconverter.core.office.OfficeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class officeConverterServiceTest {
    @Autowired
    private  DocumentConverter documentConverter;
    @Test
    public void test() throws FileNotFoundException, OfficeException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        String inputFilePath = "/Users/sunchen/IdeaProjects/coffice/file/uploadPath/22.xlsx";
        String outputFilePath = "/Users/sunchen/IdeaProjects/coffice/file/pdfPath/22.pdf";
        inputStream = new FileInputStream(inputFilePath);
        outputStream = new FileOutputStream(outputFilePath);
        documentConverter.convert(inputStream).as(DefaultDocumentFormatRegistry.XLSX)
                    .to(outputStream).as(DefaultDocumentFormatRegistry.PDF)
                    .execute();

//        documentConverter.convert(inputStream).as(DefaultDocumentFormatRegistry.XLSX)
//                    .to(outputStream).as(DefaultDocumentFormatRegistry.PDF)
//                    .execute();
    }
//    @Test
//    public void test2() throws FileNotFoundException, OfficeException {
//        InputStream inputStream = null;
//        OutputStream outputStream = null;
//        String inputFilePath = "/Users/sunchen/IdeaProjects/coffice/src/test/resources/22.pdf";
//        String outputFilePath = "/Users/sunchen/IdeaProjects/coffice/src/test/resources/22.png";
//        inputStream = new FileInputStream(inputFilePath);
//        outputStream = new FileOutputStream(outputFilePath);
//        documentConverter.convert(inputStream).as(DefaultDocumentFormatRegistry.PDF)
//                .to(outputStream).as(DefaultDocumentFormatRegistry.PNG)
//                .execute();
//    }

}