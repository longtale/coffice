package com.sunchen.coffice.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
@Data
/**
 * pdf 转换后的文档
 **/
public class PdfTransFile {
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 转换前的文档id
     */
    private Long sourceId;
    private Long userId;
    private String name;
    private String saveName;
    private String savePath;
    private Long size;
    @CreationTimestamp
    private Timestamp createTime;
    @UpdateTimestamp
    private Timestamp updateTime;
}
