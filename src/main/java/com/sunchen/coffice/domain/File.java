package com.sunchen.coffice.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
@Data

/**
 *  文件上传存储表
 */
public class File {
    @Id
    @GeneratedValue
    private Long id;
    private Long userId;
    private String name;
    private String saveName;
    private String savePath;
    private String ext;
    private String mime;
    private Long size;
    /**
     * 0- 已上传
     * 1- 已转pdf
     * 2- 已转png
     */
    private int transStatus;
    @CreationTimestamp
    private Timestamp createTime;
    @UpdateTimestamp
    private Timestamp updateTime;
}
