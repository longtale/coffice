package com.sunchen.coffice.service;

import lombok.extern.log4j.Log4j2;
import org.jodconverter.core.DocumentConverter;
import org.jodconverter.core.document.DefaultDocumentFormatRegistry;
import org.jodconverter.core.office.OfficeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.*;

@Service
@Log4j2
public class OfficeConverterService {
    @Autowired
    private DocumentConverter documentConverter;

    /**
     * 将office文档转换为pdf文档
     * @param officePath 原文档保存路径
     * @param ext 文档扩展名
     * @param pdfPath pdf文档保存路径
     * @return 成功失败信息
     * @throws FileNotFoundException 文件未找到异常
     * @throws OfficeException office转换异常
     */
    public String officeToPdf(String officePath,String ext,String pdfPath) throws FileNotFoundException, OfficeException {
        InputStream inputStream = new FileInputStream(officePath);
        OutputStream outputStream = new FileOutputStream(pdfPath);
        documentConverter
                .convert(inputStream).as(DefaultDocumentFormatRegistry.getFormatByExtension(ext))
                .to(outputStream).as(DefaultDocumentFormatRegistry.PDF)
                .execute();
        return "success";
    }
//    public String
}
