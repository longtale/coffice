package com.sunchen.coffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfficeConverterApplication {
    public static void main(String[] args){
        SpringApplication.run(OfficeConverterApplication.class,args);
    }
}