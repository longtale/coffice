package com.sunchen.coffice.common;

public class ResultUtil {

    /**
     * 返回操作成功信息
     * @param data 相关数据
     */
    public static <T> ResultResponse<T> success(T data){
        return new ResultResponse<T>(ResultCode.SUCCESS.getCode(),ResultCode.SUCCESS.getMessage(),data);
    }

    /**
     * 自定义信息返回操作成功信息，不带数据
     * @param message 自定义信息
     */
    public static <T> ResultResponse<T> success(String message){
        return new ResultResponse<T>(ResultCode.SUCCESS.getCode(),message,null);
    }
    /**
     * 自定义信息返回操作成功信息，带数据
     * @param message 自定义信息
     */
    public static <T> ResultResponse<T> success(String message,T data){
        return new ResultResponse<T>(ResultCode.SUCCESS.getCode(),message,data);
    }
    /**
     * 返回失败信息，根据错误代码自动返回，不带数据
     * @param errorCode 错误信息代码
     */
    public static <T> ResultResponse<T> failed(IErrorCode errorCode){
        return new ResultResponse<T>(errorCode.getCode(),errorCode.getMessage(),null);
    }
    /**
     * 返回失败信息，自定义返回信息
     * @param errorCode 错误信息代码
     * @param message 自定义错误信息
     */
    public static <T> ResultResponse<T> failed(IErrorCode errorCode,String message){
        return new ResultResponse<T>(errorCode.getCode(),message,null);
    }

    /**
     * 返回失败信息，仅含信息
     * @param message
     */
    public static <T> ResultResponse<T> failed(String message){
        return new ResultResponse<T>(ResultCode.FAILED.getCode(),message,null);
    }
    /**
     * 返回失败信息，无参数
     */
    public static <T> ResultResponse<T> failed(){
        return failed(ResultCode.FAILED);
    }
    /**
     * 参数验证失败错误,无参数
     */
    public static <T> ResultResponse<T> validateFailed(){
        return failed(ResultCode.VALIDATE_FAILED);
    }
    /**
     * 未登录错误，无参数
     */
    public static <T> ResultResponse<T> unauthorized(){
        return failed(ResultCode.UNAUTHORIZED);
    }
    /**
     * 未授权错误，无参数
     */
    public static <T> ResultResponse<T> forbidden(){
        return failed(ResultCode.FORBIDDEN);
    }


}
