package com.sunchen.coffice.common;

public interface IErrorCode {
    Integer getCode();
    String getMessage();
}
