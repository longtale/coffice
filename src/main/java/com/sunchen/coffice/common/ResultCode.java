package com.sunchen.coffice.common;

public enum ResultCode implements IErrorCode{
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    FORBIDDEN(403, "没有相关权限"),


    BAD_REQUEST(400,"服务器不理解客户端的请求，未做任何处理"),
    NOT_FOUND(404,"所请求的资源不存在，或不可用"),
    METHOD_NOT_ALLOWED(405,"用户没有该功能权限"),
    GONE(410,"请求的资源不可用"),
    UN_SUPPORTED(415,"客户端要求的返回格式不支持"),
    UN_PROCESSABLE(422,"客户端上传的附件无法处理"),
    TOO_MANY_REQUEST(429,"客户端的请求次数超过限额");
    //暂定

    private Integer code;
    private String message;
    private ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
