package com.sunchen.coffice.repository;

import com.sunchen.coffice.domain.ViewFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViewFileRepository extends JpaRepository<ViewFile,Long> {
}
