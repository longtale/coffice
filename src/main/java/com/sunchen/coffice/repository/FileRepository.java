package com.sunchen.coffice.repository;

import com.sunchen.coffice.domain.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<File,Long> {

}
