package com.sunchen.coffice.repository;

import com.sunchen.coffice.domain.PdfTransFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PdfTransFileRepository extends JpaRepository<PdfTransFile,Long> {
}
