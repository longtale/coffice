package com.sunchen.coffice.rest;

import com.sunchen.coffice.common.ResultResponse;
import com.sunchen.coffice.common.ResultUtil;
import com.sunchen.coffice.domain.File;
import com.sunchen.coffice.domain.PdfTransFile;
import com.sunchen.coffice.repository.FileRepository;
import com.sunchen.coffice.repository.PdfTransFileRepository;
import com.sunchen.coffice.service.OfficeConverterService;
import lombok.extern.slf4j.Slf4j;
import org.jodconverter.core.office.OfficeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;

@RestController
@RequestMapping("/")
@Slf4j
public class OfficeConverterRest {
    @Autowired
    private OfficeConverterService officeConverterService;
    @Autowired
    private PdfTransFileRepository pdfTransFileRepository;
    @Autowired
    private FileRepository fileRepository;
    @Value("${filePath.uploadPath}")
    private String uploadPath;
    @Value("${filePath.pdfPath}")
    private String pdfPath;
    @Value("${filePath.pngPath}")
    private String pngPath;
    @RequestMapping(value="file/trans" , method = RequestMethod.POST)
    public ResultResponse fileTrans(@RequestParam("file") MultipartFile file) throws IOException {
        //文件上传
        //存储原始文件及相关信息
        File fileInfo = new File();
        //获取上传文件的原始名称
        String fileName= file.getOriginalFilename();
        //生成新的文件名
        String ext = fileName.substring(fileName.lastIndexOf(".")+1).toLowerCase();
        String saveName = Timestamp.valueOf(LocalDateTime.now()).getTime()+"."+ext;
        String savePath = uploadPath+saveName;
        log.info(savePath);
        fileInfo.setName(fileName);
        fileInfo.setExt(ext);
        fileInfo.setSaveName(saveName);
        fileInfo.setSavePath(savePath);
        java.io.File fileSaveHandle = new java.io.File(savePath);
        file.transferTo(fileSaveHandle);
        fileRepository.save(fileInfo);
        log.info("文件上传成功，文件信息:{}",fileInfo);
        //生成pdf存储路径
        String pdfFileName = Timestamp.valueOf(LocalDateTime.now()).getTime()+"";
        String pdfSavePath = pdfPath+pdfFileName+".pdf";
        //@todo 判断后缀名是否为office文档格式
        //如果是office文档格式，先转换为pdf格式并存储文件和存储信息
        log.info("开始转换为pdf格式");
        try {
            officeConverterService.officeToPdf(savePath,ext,pdfSavePath);
            log.info("文件转换成功，文件转换信息："+pdfSavePath);
            PdfTransFile pdfTransFile = new PdfTransFile();
            pdfTransFile.setName(fileName);
            pdfTransFile.setSaveName(pdfFileName);
            pdfTransFile.setSavePath(pdfSavePath);
            pdfTransFileRepository.save(pdfTransFile);
            return ResultUtil.success("文件转换成功",pdfTransFile);
        }catch (OfficeException e){
            log.info("文件转换失败，源文件信息:{}",fileInfo);
            return ResultUtil.success("文件转换失败");
        }
        //再次将pdf转换为图片格式，并存储文件和信息
    }
//    @RequestMapping(value = "test",method = RequestMethod.GET)
//    public String test()
//    {
//        String wordPath = "/Users/sunchen/IdeaProjects/coffice/src/test/resources/22.xlsx";
//        String pdfPath = "/Users/sunchen/IdeaProjects/coffice/src/test/resources/22.pdf";
//        try {
//            return officeConverterService.wordToPdf(wordPath,pdfPath);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            return e.getMessage();
//        } catch (OfficeException e) {
//            e.printStackTrace();
//            return e.getMessage();
//        }
//    }
}
